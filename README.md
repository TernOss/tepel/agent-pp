# agent++ RPM build spec

[![Copr build status](https://copr.fedorainfracloud.org/coprs/laridae/snmp_pp/package/agent++/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/laridae/snmp_pp/package/agent++/)

See further:
https://agentpp.com/api/cpp/agent_pp.html
