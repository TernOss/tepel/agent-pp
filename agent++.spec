Name:          agent++
Version:       4.6.1
Release:       1%{?dist}
Summary:       AGENT++ API
License:       Apache-2.0
Group:         Development/Libraries/C and C++

Url:           https://agentpp.com/api/cpp/agent_pp.html
Source0:       https://agentpp.com/download/%{name}-%{version}.tar.gz
Patch0:        agent-pp-cmakelists.patch

%if 0%{?suse_version}
BuildRequires: cmake gcc-c++ libopenssl-1_1-devel snmp++-devel
%else
BuildRequires: cmake gcc-c++ openssl-devel snmp++-devel
%endif

%description
AGENT++ supports Linux, Solaris, HPUX, AIX, and Windows XP, 7, and 8 (VS 2010-2015) and requires SNMP++ version 3.x or higher. Because AGENT++ is ANSI C++ compliant, it may be used with other platforms and many embedded OS as well.

A list of the RFCs (and MIBs) implemented by SNMP++ and AGENT++ can be found here.

It can be configured at compile time whether to support SNMPv3 features or not. By disabling SNMPv3 support the View Access Model and the User based Security Model are also disabled! In this case AGENT++ only supports one read and one write community providing no security at all. However, there might be a situation where security is not needed, but the performance and resources gained from disabling SNMPv3 are more valuable.

For any regular use, it is highly recommended to activate and use the SNMPv3 support.

%package devel
Summary: AGENT++ development files
Requires: %{name} = %{version}-%{release}
Requires: snmp++-devel

%description devel
This package contains necessary header files for AGENT++ development.

%prep
%setup
%patch0

%build
%cmake -DCMAKE_INSTALL_LIBDIR=%{_libdir}
%cmake_build


%install
%if 0%{?suse_version} >= 15
cd build
%endif
%cmake_install

%files
%defattr(-,root,root,-)
%doc
%{_libdir}/libagent++.so*

%files devel
%defattr(-,root,root,-)
%doc
%{_includedir}/libagent.h
%{_includedir}/agent_pp/*
%{_libdir}/*.a

%changelog
* Tue Nov 12 2024 Ari Guðfinnsson <ari@tern.is> 4.6.1-1
Update to version 4.6.1

